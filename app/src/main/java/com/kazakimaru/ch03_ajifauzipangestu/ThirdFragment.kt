package com.kazakimaru.ch03_ajifauzipangestu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import com.kazakimaru.ch03_ajifauzipangestu.SecondFragment.Companion.NAMA
import com.kazakimaru.ch03_ajifauzipangestu.SecondFragment.Companion.PARCELABLE_KEY
import com.kazakimaru.ch03_ajifauzipangestu.SecondFragment.Companion.REQUEST_KEY
import com.kazakimaru.ch03_ajifauzipangestu.databinding.FragmentThirdBinding


class ThirdFragment : Fragment() {

    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(REQUEST_KEY) { _, bundle ->
            val getPerson = bundle.getParcelable<Person>(PARCELABLE_KEY) as Person
            val getUsia = getPerson.usia
            val getAlamat = getPerson.alamat
            val getPekerjaan = getPerson.pekerjaan

            val oddEven = getGanjilGenap(getUsia)

            binding.txtUsia.isGone = false
            binding.txtAlamat.isGone = false
            binding.txtPekerjaan.isGone = false

            binding.txtUsia.text = "Usia: " + getUsia.toString() + " ($oddEven)"
            binding.txtAlamat.text = "Alamat: " + getAlamat
            binding.txtPekerjaan.text = "Pekerjaan: " + getPekerjaan
        }
    }

    private fun getGanjilGenap(usia: Int): String {
        if(usia % 2 == 0) {
            return "Genap"
        } else {
            return "Ganjil"
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getNama()
        moveToScreen4()
    }

    private fun getNama() {
        val getNama = arguments?.getString(NAMA)
        binding.txtNama.text = "Nama: " + getNama
    }

    private fun moveToScreen4(){
        binding.btnTo4.setOnClickListener {
            it.findNavController().navigate(R.id.action_thirdFragment_to_fourthFragment)
        }
    }

}