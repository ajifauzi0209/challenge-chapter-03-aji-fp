package com.kazakimaru.ch03_ajifauzipangestu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.kazakimaru.ch03_ajifauzipangestu.databinding.FragmentSecondBinding


class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    companion object {
        val NAMA = "nama"
        val REQUEST_KEY = "request"
        val PARCELABLE_KEY = "parcelable"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moveToScreen3()
    }

    private fun moveToScreen3() {
        binding.btnTo3.setOnClickListener {
            val getEditTextNama = binding.editNama.editText?.text.toString()
            val bundle = Bundle()
            bundle.putString(NAMA, getEditTextNama)
            it.findNavController().navigate(R.id.action_secondFragment_to_thirdFragment, bundle)
        }
    }
}