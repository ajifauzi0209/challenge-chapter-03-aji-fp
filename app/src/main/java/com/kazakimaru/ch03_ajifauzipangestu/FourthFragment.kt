package com.kazakimaru.ch03_ajifauzipangestu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import com.kazakimaru.ch03_ajifauzipangestu.SecondFragment.Companion.PARCELABLE_KEY
import com.kazakimaru.ch03_ajifauzipangestu.SecondFragment.Companion.REQUEST_KEY
import com.kazakimaru.ch03_ajifauzipangestu.databinding.FragmentFourthBinding


class FourthFragment : Fragment() {

    private var _binding: FragmentFourthBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFourthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sendDataToScreen3()
    }

    private fun sendDataToScreen3() {
        binding.backTo3.setOnClickListener {
            val getEditTextUsia = binding.editUsia.editText?.text.toString().toInt()
            val getEditTextAlamat = binding.editAlamat.editText?.text.toString()
            val getEditTextPekerjaan = binding.editPekerjaan.editText?.text.toString()

            val person = Person(getEditTextUsia, getEditTextAlamat, getEditTextPekerjaan)

            val bundle = Bundle()
            bundle.putParcelable(PARCELABLE_KEY, person)

            setFragmentResult(REQUEST_KEY, bundle)
            it.findNavController().popBackStack()
        }
    }

}