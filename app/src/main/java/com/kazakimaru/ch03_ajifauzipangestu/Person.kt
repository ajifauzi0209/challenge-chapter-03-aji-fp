package com.kazakimaru.ch03_ajifauzipangestu

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Person(
    val usia: Int,
    val alamat: String,
    val pekerjaan: String
) : Parcelable